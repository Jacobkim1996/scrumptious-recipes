from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from meal_plans.models import MealPlans
from django.contrib.auth.mixins import LoginRequiredMixin
from recipes.models import Recipe


class MealPlansListView(LoginRequiredMixin, ListView):
    model = MealPlans
    template_name = "meal_plans/list.html"
    paginate_by = 2

    def get_queryset(self):
        querystring = self.request.GET.get("q")
        if querystring is None:
            querystring = ""
        print(self.queryset)
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlans
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def create_meal_plans(self, request):
        if request.method == "POST":
            form = MealPlans(request.POST)
            if form.is_valid():
                plan = form.save(commit=False)
                plan.owner = self.request.user
                plan.save()
                form.save_m2m()
                return redirect("meal_plans_detail", pk=plan.id)


class MealPlansDetailView(DetailView):
    model = MealPlans
    template_name = "meal_plans/detail.html"
    context_object_name = "mealplan"


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlans
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans_list")


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlans
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")
