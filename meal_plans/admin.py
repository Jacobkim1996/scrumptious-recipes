from django.contrib import admin
from meal_plans.models import (
    MealPlans,
    # Measure,
    # FoodItem,
    # Ingredient,
    # Step,
    # Rating,
)

# Register your models here.


class MealPlansAdmin(admin.ModelAdmin):
    pass


# class MeasureAdmin(admin.ModelAdmin):
#     pass


# class FoodItemAdmin(admin.ModelAdmin):
#     pass


# class IngredientAdmin(admin.ModelAdmin):
#     pass


# class StepAdmin(admin.ModelAdmin):
#     pass


# class RatingAdmin(admin.ModelAdmin):
#     pass


admin.site.register(MealPlans, MealPlansAdmin)
# admin.site.register(Measure, MeasureAdmin)
# admin.site.register(FoodItem, FoodItemAdmin)
# admin.site.register(Ingredient, IngredientAdmin)
# admin.site.register(Step, StepAdmin)
# admin.site.register(Rating, RatingAdmin)
